package com.goyello;


import java.util.*;

public class MazeGenerator {

    public MazeGenerator(int size) {
        System.out.println("-------------------------------------------------------------------");
        System.out.println();

        // Constructs a new 2D array
        String[][] maze2D = maze2D(size);

        // Prints out new maze as 2D array
        String[][] mazeGenerated = generator(maze2D);

        // Prints the string representation of maze
        System.out.println(convert2D(mazeGenerated));
        System.out.println("String Representation of Generated " + size + "x" + size + " Maze");

        // Delete the Hash symbol in the maze
        mazeGenerated = emptyHash(mazeGenerated);

        // Depth First Search
        String[][] mazeDFS = DFS(clone(mazeGenerated));
        System.out.println();

        // String representation of DFS
        System.out.println(convert2D(mazeDFS));
        System.out.println("String representation of DFS Maze");
        System.out.println();

        // Breadth First Search
        String[][] mazeBFS = BFS(clone(mazeGenerated));
        System.out.println(convert2D(mazeBFS));
        System.out.println("String representation of BFS Maze");
        System.out.println();

        // Creates an single path of the maze
        String[][] mazePath = backtrackingDelete(clone(mazeGenerated));
        emptyHash(mazePath);
        hashList(mazePath);

        // Prints the string representation of maze with path
        System.out.println(printPath(mazePath));
        System.out.println("Hash Single Path");
    }

    public static void print2D(String[][] array2D) {
        for (String[] row : array2D) {
            System.out.println(Arrays.toString(row));
        }
    }

    private static String[][] clone(String[][] maze2D) {
        String[][] clone2D = new String[maze2D.length][maze2D.length];
        for (int columnIndex = 0; columnIndex < maze2D.length; columnIndex++) {
            System.arraycopy(maze2D[columnIndex], 0, clone2D[columnIndex], 0, maze2D.length);
        }
        return clone2D;
    }

    private static String[][] maze2D(int size) {
        String[][] maze2D = new String[2 * size + 1][2 * size + 1];
        // 2D Array
        for (int columnIndex = 0; columnIndex < (2 * size + 1); columnIndex++) {
            for (int rowIndex = 0; rowIndex < (2 * size + 1); rowIndex++) {
                // Start of maze
                if (rowIndex == 1 && columnIndex == 0) {
                    maze2D[columnIndex][rowIndex] = "S";
                    // End of maze
                } else if (rowIndex == 2 * size - 1 && columnIndex == 2 * size) {
                    maze2D[columnIndex][rowIndex] = "E";
                    // Row is even
                } else if (rowIndex % 2 == 0) {
                    // Column is even
                    if (columnIndex % 2 == 0) {
                        maze2D[columnIndex][rowIndex] = "+";
                        // Column is odd
                    } else {
                        maze2D[columnIndex][rowIndex] = "|";
                    }
                    // Row is odd
                } else {
                    // Column is even
                    if (columnIndex % 2 == 0) {
                        maze2D[columnIndex][rowIndex] = "-";
                        // Column is odd
                    } else {
                        maze2D[columnIndex][rowIndex] = "0";
                    }
                }
            }
        }

        return maze2D;
    }

    private static String[][] generator(String[][] maze2D) {
        Stack<Cell> location = new Stack<>();
        int size = (maze2D.length - 1) / 2;
        int totalCells = size * size;
        int visitedCells = 1;
        Cell current = new Cell(0, 0);

        while (visitedCells < totalCells) {

            // Generates a unique direction
            ArrayList<String> direction = new ArrayList<>();
            Collections.addAll(direction, "NORTH", "EAST", "SOUTH", "WEST");
            Collections.shuffle(direction);

            String random = validSpot(maze2D, current, direction);

            if (random.equals("BACKTRACK")) {
                // // DEBUGGING: Prints BACKTRACKING
                // System.out.println("\t PROCESSS: " + random);

                current = location.pop();
                continue;
            }

            current = move(maze2D, current, random);
            visitedCells = visitedCells + 1;
            location.push(current);
        }

        return maze2D;
    }

    private static String validSpot(String[][] maze2D, Cell current, ArrayList<String> direction) {
        int size = (maze2D.length - 1) / 2;

        int x = 2 * current.getx() + 1;
        int y = 2 * current.gety() + 1;

        // When the size of the list is 0, return -1
        if (direction.size() == 0) {
            return "BACKTRACK";
        }

        String random = direction.remove(0);

        // // DEBUGGING: Prints current direction
        // System.out.println("DIRECTION: " + random);

        switch (random) {
            case "NORTH":
                if (current.gety() - 1 < 0) {
                    // System.out.println("Do not go NORTH because outside of range of the 2D
                    // array");
                    return validSpot(maze2D, current, direction);
                }
                if ((maze2D[y - 3][x].equals("#") || maze2D[y - 1][x].equals("#"))
                        || (maze2D[y - 2][x - 1].equals("#") || maze2D[y - 2][x + 1].equals("#"))) {
                    // System.out.println("Do not go NORTH because that cell is not enclosed by
                    // walls");
                    return validSpot(maze2D, current, direction);
                }
                break;
            case "EAST":
                if (current.getx() + 1 >= size) {
                    // System.out.println("Do not go EAST because outside of range of the 2D
                    // array");
                    return validSpot(maze2D, current, direction);
                }
                if (((maze2D[y + 1][x + 2].equals("#") || maze2D[y - 1][x + 2].equals("#"))
                        || (maze2D[y][x + 1].equals("#") || maze2D[y][x + 3].equals("#")))) {
                    // System.out.println("Do not go EAST because that cell is not enclosed by
                    // walls");
                    return validSpot(maze2D, current, direction);
                }
                break;
            case "SOUTH":
                if (current.gety() + 1 >= size) {
                    // System.out.println("Do not go SOUTH because outside of range of the 2D
                    // array");
                    return validSpot(maze2D, current, direction);
                }
                if (((maze2D[y + 1][x].equals("#") || maze2D[y + 3][x].equals("#"))
                        || (maze2D[y + 2][x - 1].equals("#") || maze2D[y + 2][x + 1].equals("#")))) {
                    // System.out.println("Do not go SOUTH because that cell is not enclosed by
                    // walls");
                    return validSpot(maze2D, current, direction);
                }
                break;
            case "WEST":
                if (current.getx() - 1 < 0) {
                    // System.out.println("Do not go WEST because outside of range of the 2D
                    // array");
                    return validSpot(maze2D, current, direction);
                }
                if (((maze2D[y - 1][x - 2].equals("#") || maze2D[y + 1][x - 2].equals("#"))
                        || (maze2D[y][x - 3].equals("#") || maze2D[y][x - 1].equals("#")))) {
                    // System.out.println("Do not go WEST because that cell is not enclosed by
                    // walls");
                    return validSpot(maze2D, current, direction);
                }
                break;
        }
        return random;
    }

    private static Cell move(String[][] maze2D, Cell current, String random) {

        // // Prints out the coordinates of the current cell object
        // System.out.println(" X-coordinate: " + current.getx() + ", Y-coordinate: " +
        // current.gety());

        maze2D[1][1] = "#";

        switch (random) {
            case "NORTH":
                // NORTH and delete wall from bottom from next cell
                current.setNext(new Cell(current.getx(), current.gety() - 1));
                current = current.getNext();
                // Breaks the bottom wall from next cell
                maze2D[2 * current.gety() + 2][2 * current.getx() + 1] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";
                break;
            case "EAST":
                // EAST and delete wall from left from next cell
                current.setNext(new Cell(current.getx() + 1, current.gety()));
                current = current.getNext();
                // Breaks the left wall from next cell
                maze2D[2 * current.gety() + 1][2 * current.getx()] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";
                break;
            case "SOUTH":
                // SOUTH and delete wall from top from next cell
                current.setNext(new Cell(current.getx(), current.gety() + 1));
                current = current.getNext();
                // Breaks the top wall from next cell
                maze2D[2 * current.gety()][2 * current.getx() + 1] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";
                break;
            case "WEST":
                // WEST and delete wall from right from next cell
                current.setNext(new Cell(current.getx() - 1, current.gety()));
                current = current.getNext();
                // Breaks the right wall from next cell
                maze2D[2 * current.gety() + 1][2 * current.getx() + 2] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";
                break;
        }

        // // DEBUGGING: Printing maze at each step
        // System.out.println("NEW X-coordinate: " + current.getx() + ", NEW
        // Y-coordinate: " + current.gety());
        // for (String[] row : maze2D) {
        // System.out.println(Arrays.toString(row));
        // }
        // System.out.println();

        return current;
    }

    public int visitedCells(int size) {
        return size*size;
    }

    private static String convert2D(String[][] maze2D) {
        StringBuilder maze = new StringBuilder();
        int size = maze2D.length;
        for (int columnIndex = 0; columnIndex < size; columnIndex++) {
            for (int rowIndex = 0; rowIndex < size; rowIndex++) {
                if (maze2D[columnIndex][rowIndex].equals("+")) {
                    maze.append("+");
                } else if (maze2D[columnIndex][rowIndex].equals("-")) {
                    maze.append("---");
                } else if (maze2D[columnIndex][rowIndex].equals("|")) {
                    maze.append("|");
                } else if (maze2D[columnIndex][rowIndex].equals("#") && columnIndex % 2 == 1) {
                    // Hash symbol and column is odd
                    if (rowIndex % 2 == 0) {
                        maze.append(" ");
                    } else if (rowIndex % 2 == 1) {
                        maze.append("   ");
                    }
                } else if (maze2D[columnIndex][rowIndex].equals("#") && columnIndex % 2 == 0) {
                    // Hash symbol and column is even
                    maze.append("   ");
                } else if (maze2D[columnIndex][rowIndex].equals("S") || maze2D[columnIndex][rowIndex].equals("E")) {
                    maze.append("   ");
                } else if (maze2D[columnIndex][rowIndex].equals(" ") && columnIndex % 2 == 1 && rowIndex % 2 == 0) {
                    // Spacing for the wall
                    maze.append(" ");
                } else if (maze2D[columnIndex][rowIndex].equals(" ")) {
                    // Spacing for the cell
                    maze.append("   ");
                } else {
                    maze.append(" ").append(maze2D[columnIndex][rowIndex]).append(" ");
                }

                // When rowIndex is at end AND columnIndex is not at end, add a new line
                if (rowIndex == (size - 1) && columnIndex != (size - 1)) {
                    maze.append(System.lineSeparator());
                }
            }
        }
        return maze.toString();
    }

    private static String[][] emptyHash(String[][] maze2D) {
        int size = maze2D.length;
        for (int columnIndex = 0; columnIndex < size; columnIndex++) {
            for (int rowIndex = 0; rowIndex < size; rowIndex++) {
                if (maze2D[columnIndex][rowIndex].equals("#")) {
                    maze2D[columnIndex][rowIndex] = " ";
                }
            }
        }
        return maze2D;
    }

    private static String[][] DFS(String[][] maze2D) {
        Stack<Cell> location = new Stack<>();
        int size = (maze2D.length - 1) / 2;
        int totalCells = size * size;
        int visitedCells = 1;
        Cell current = new Cell(0, 0);
        maze2D[1][1] = "0";

        while (visitedCells < totalCells) {
            // Generates a unique direction
            ArrayList<String> direction = new ArrayList<>();
            Collections.addAll(direction, "NORTH", "EAST", "SOUTH", "WEST");
            Collections.shuffle(direction);

            // Finds a valid spot on the 2D array
            String random = DFSValid(maze2D, current, direction);
            // System.out.println("The FINAL DIRECTION: " + random);

            if (random.equals("BACKTRACK")) {
                current = location.pop();
                continue;
            }

            current = DFSMove(maze2D, current, random, visitedCells);
            visitedCells = visitedCells + 1;
            location.push(current);

            if (current.getx() == size - 1 && current.gety() == size - 1) {
                return maze2D;
            }
        }

        return maze2D;
    }

    private static String DFSValid(String[][] maze2D, Cell current, ArrayList<String> direction) {
        int size = (maze2D.length - 1) / 2;
        int x = 2 * current.getx() + 1;
        int y = 2 * current.gety() + 1;

        // When the size of the list is 0, return "BACKTRACK"
        if (direction.size() == 0) {
            // maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = " ";
            return "BACKTRACK";
        }

        String random = direction.remove(0);

        switch (random) {
            case "NORTH":
                if (current.gety() - 1 < 0) {
                    // System.out.println("Do not go NORTH because outside of range of the 2D
                    // array");
                    return DFSValid(maze2D, current, direction);
                }
                if (!maze2D[y - 1][x].equals(" ")) {
                    // System.out.println("Do not go NORTH because there is a wall");
                    return DFSValid(maze2D, current, direction);
                }
                break;
            case "EAST":
                if (current.getx() + 1 >= size) {
                    // System.out.println("Do not go EAST because outside of range of the 2D
                    // array");
                    return DFSValid(maze2D, current, direction);
                }
                if (!maze2D[y][x + 1].equals(" ")) {
                    // System.out.println("Do not go EAST because there is a wall");
                    return DFSValid(maze2D, current, direction);
                }
                break;
            case "SOUTH":
                if (current.gety() + 1 >= size) {
                    // System.out.println("Do not go SOUTH because outside of range of the 2D
                    // array");
                    return DFSValid(maze2D, current, direction);
                }
                if (!maze2D[y + 1][x].equals(" ")) {
                    // System.out.println("Do not go SOUTH because there is a wall");
                    return DFSValid(maze2D, current, direction);
                }
                break;
            case "WEST":
                if (current.getx() - 1 < 0) {
                    // System.out.println("Do not go WEST because outside of range of the 2D
                    // array");
                    return DFSValid(maze2D, current, direction);
                }
                if (!maze2D[y][x - 1].equals(" ")) {
                    // System.out.println("Do not go WEST because there is a wall");
                    return DFSValid(maze2D, current, direction);
                }
                break;
        }
        return random;
    }

    private static Cell DFSMove(String[][] maze2D, Cell current, String random, int count) {

        String path = Integer.toString(count % 10);

        switch (random) {
            case "NORTH":
                // NORTH and delete wall from bottom from next cell
                current.setNext(new Cell(current.getx(), current.gety() - 1));
                current = current.getNext();
                // Breaks the bottom wall from next cell
                maze2D[2 * current.gety() + 2][2 * current.getx() + 1] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = path;
                break;
            case "EAST":
                // EAST and delete wall from left from next cell
                current.setNext(new Cell(current.getx() + 1, current.gety()));
                current = current.getNext();
                // Breaks the left wall from next cell
                maze2D[2 * current.gety() + 1][2 * current.getx()] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = path;
                break;
            case "SOUTH":
                // SOUTH and delete wall from top from next cell
                current.setNext(new Cell(current.getx(), current.gety() + 1));
                current = current.getNext();
                // Breaks the top wall from next cell
                maze2D[2 * current.gety()][2 * current.getx() + 1] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = path;
                break;
            case "WEST":
                // WEST and delete wall from right from next cell
                current.setNext(new Cell(current.getx() - 1, current.gety()));
                current = current.getNext();
                // Breaks the right wall from next cell
                maze2D[2 * current.gety() + 1][2 * current.getx() + 2] = "#";

                // DEBUGGING: Visualizing
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = path;
                break;
        }
        return current;

    }

    private static String[][] BFS(String[][] maze2D) {
        Queue<Cell> neighborQueue = new LinkedList<>();

        int size = (maze2D.length - 1) / 2;
        int totalCells = size * size;
        int visitedCells = 1;
        Cell current = new Cell(0, 0);
        neighborQueue.add(current);
        maze2D[1][1] = "0";

        while (visitedCells < totalCells) {
            ArrayList<String> direction;

            direction = BFSValid(maze2D, current);
            current = BFSMove(maze2D, current, neighborQueue, direction, visitedCells);
            visitedCells = visitedCells + 1;

            if (current.getx() == size - 1 && current.gety() == size - 1) {
                return maze2D;
            }
        }
        return maze2D;
    }
    private static ArrayList<String> BFSValid(String[][] maze2D, Cell current) {
        int size = (maze2D.length - 1) / 2;
        int x = 2 * current.getx() + 1;
        int y = 2 * current.gety() + 1;

        // Generates a unique direction
        ArrayList<String> direction = new ArrayList<>();
        Collections.addAll(direction, "NORTH", "EAST", "SOUTH", "WEST");

        // Removes NORTH
        if (current.gety() - 1 < 0) {
            // System.out.println("Do not go NORTH because outside of range of the 2D
            // array");
            direction.remove("NORTH");
        } else if (!maze2D[y - 1][x].equals(" ") || !maze2D[y - 2][x].equals(" ")) {
            // System.out.println("Do not go NORTH because there is a wall");
            direction.remove("NORTH");
        }
        // Removes EAST
        if (current.getx() + 1 >= size) {
            // System.out.println("Do not go EAST because outside of range of the 2D
            // array");
            direction.remove("EAST");
        } else if (!maze2D[y][x + 1].equals(" ") || !maze2D[y][x + 2].equals(" ")) {
            // System.out.println("Do not go EAST because there is a wall");
            direction.remove("EAST");
        }
        // Removes SOUTH
        if (current.gety() + 1 >= size) {
            // System.out.println("Do not go SOUTH because outside of range of the 2D
            // array");
            direction.remove("SOUTH");
        } else if (!maze2D[y + 1][x].equals(" ") || !maze2D[y + 2][x].equals(" ")) {
            // System.out.println("Do not go SOUTH because there is a wall");
            direction.remove("SOUTH");
        }
        // Removes WEST
        if (current.getx() - 1 < 0) {
            // System.out.println("Do not go WEST because outside of range of the 2D
            // array");
            direction.remove("WEST");
        } else if (!maze2D[y][x - 1].equals(" ") || !maze2D[y][x - 2].equals(" ")) {
            // System.out.println("Do not go WEST because there is a wall");
            direction.remove("WEST");
        }

        Collections.shuffle(direction);

        return direction;
    }
    private static Cell BFSMove(String[][] maze2D, Cell current, Queue<Cell> neighborQueue, ArrayList<String> direction,
                                int count) {

        String path = Integer.toString(count % 10);

        while (direction.size() > 0) {
            // System.out.println("Enters while loop");

            String random = direction.remove(0);
            switch (random) {
                case "NORTH":
                    // System.out.println("Removes NORTH");
                    neighborQueue.add(new Cell(current.getx(), current.gety() - 1));
                    break;
                case "EAST":
                    // System.out.println("Removes EAST");
                    neighborQueue.add(new Cell(current.getx() + 1, current.gety()));
                    break;
                case "SOUTH":
                    // System.out.println("Removes SOUTH");
                    neighborQueue.add(new Cell(current.getx(), current.gety() + 1));
                    break;
                case "WEST":
                    // System.out.println("Removes WEST");
                    neighborQueue.add(new Cell(current.getx() - 1, current.gety()));
                    break;
            }
        }
        neighborQueue.remove();
        current = neighborQueue.peek();
        if (current != null) {
            maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = path;
        }

        return current;
    }
    private static String[][] backtrackingDelete(String[][] maze2D) {
        Stack<Cell> location = new Stack<>();
        int size = (maze2D.length - 1) / 2;
        int totalCells = size * size;
        int visitedCells = 1;
        Cell current = new Cell(0, 0);
        maze2D[1][1] = "0";

        while (visitedCells < totalCells) {
            // Generates a unique direction
            ArrayList<String> direction = new ArrayList<>();
            Collections.addAll(direction, "NORTH", "EAST", "SOUTH", "WEST");
            Collections.shuffle(direction);

            // Finds a valid spot on the 2D array
            String random = DFSValid(maze2D, current, direction);
            // System.out.println("The FINAL DIRECTION: " + random);

            if (random.equals("BACKTRACK")) {
                maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = " ";
                current = location.pop();
                continue;
            }

            current = DFSMove(maze2D, current, random, visitedCells);
            visitedCells = visitedCells + 1;
            location.push(current);

            if (current.getx() == size - 1 && current.gety() == size - 1) {
                return maze2D;
            }
        }
        return maze2D;
    }
    public static ArrayList<Cell> hashList(String[][] maze2D) {
        int size = (maze2D.length - 1) / 2;
        ArrayList<Cell> path = new ArrayList<>();
        Cell current = new Cell(0, 0);
        path.add(current);

        while (current.getx() != size - 1 || current.gety() != size - 1) {

            // NORTH
            if (current.gety() - 1 < 0) {
                // Do Nothing
            } else if (maze2D[2 * current.gety()][2 * current.getx() + 1].equals(" ")
                    && !maze2D[2 * current.gety() - 1][2 * current.getx() + 1].equals(" ")
                    && !maze2D[2 * current.gety() - 1][2 * current.getx() + 1].equals("#")) {

                path.add(new Cell(current.getx(), current.gety() - 1));
                current.setNext(new Cell(current.getx(), current.gety() - 1));

            }

            // EAST
            if (current.getx() + 1 >= size) {
                // Do Nothing
            } else if (maze2D[2 * current.gety() + 1][2 * current.getx() + 2].equals(" ")
                    && !maze2D[2 * current.gety() + 1][2 * current.getx() + 3].equals(" ")
                    && !maze2D[2 * current.gety() + 1][2 * current.getx() + 3].equals("#")) {

                path.add(new Cell(current.getx() + 1, current.gety()));
                current.setNext(new Cell(current.getx() + 1, current.gety()));
            }

            // SOUTH
            if (current.gety() + 1 >= size) {
                // Do Nothing
            } else if (maze2D[2 * current.gety() + 2][2 * current.getx() + 1].equals(" ")
                    && !maze2D[2 * current.gety() + 3][2 * current.getx() + 1].equals(" ")
                    && !maze2D[2 * current.gety() + 3][2 * current.getx() + 1].equals("#")) {

                path.add(new Cell(current.getx(), current.gety() + 1));
                current.setNext(new Cell(current.getx(), current.gety() + 1));
            }

            // WEST
            if (current.getx() - 1 < 0) {
                // Do Nothing
            } else if (maze2D[2 * current.gety() + 1][2 * current.getx()].equals(" ")
                    && !maze2D[2 * current.gety() + 1][2 * current.getx() - 1].equals(" ")
                    && !maze2D[2 * current.gety() + 1][2 * current.getx() - 1].equals("#")) {

                path.add(new Cell(current.getx() - 1, current.gety()));
                current.setNext(new Cell(current.getx() - 1, current.gety()));
            }

            maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";
            current = current.getNext();
        }

        maze2D[2 * current.gety() + 1][2 * current.getx() + 1] = "#";

        // Deletes all the extra numbers
        for (int columnIndex = 0; columnIndex < maze2D.length; columnIndex++) {
            for (int rowIndex = 0; rowIndex < maze2D.length; rowIndex++) {
                if (!(maze2D[columnIndex][rowIndex].equals("+") || maze2D[columnIndex][rowIndex].equals("-")
                        || maze2D[columnIndex][rowIndex].equals("|") || maze2D[columnIndex][rowIndex].equals("#"))) {
                    maze2D[columnIndex][rowIndex] = " ";
                }
            }
        }

        return path;
    }
    private static String printPath(String[][] maze2D) {
        StringBuilder maze = new StringBuilder();
        int size = maze2D.length;
        for (int columnIndex = 0; columnIndex < size; columnIndex++) {
            for (int rowIndex = 0; rowIndex < size; rowIndex++) {
                if (maze2D[columnIndex][rowIndex].equals("+")) {
                    maze.append("+");
                } else if (maze2D[columnIndex][rowIndex].equals("-")) {
                    maze.append("---");
                } else if (maze2D[columnIndex][rowIndex].equals("|")) {
                    maze.append("|");
                } else if (maze2D[columnIndex][rowIndex].equals("#") && columnIndex % 2 == 1) {
                    // Hash symbol and column is odd
                    if (rowIndex % 2 == 0) {
                        maze.append(" ");
                    } else if (rowIndex % 2 == 1) {
                        maze.append(" # ");
                    }
                } else if (maze2D[columnIndex][rowIndex].equals("#") && columnIndex % 2 == 0) {
                    // Hash symbol and column is even
                    maze.append(" # ");
                } else if (maze2D[columnIndex][rowIndex].equals("S") || maze2D[columnIndex][rowIndex].equals("E")) {
                    maze.append("   ");
                } else if (maze2D[columnIndex][rowIndex].equals(" ") && columnIndex % 2 == 1 && rowIndex % 2 == 0) {
                    // Spacing for the wall
                    maze.append(" ");
                } else if (maze2D[columnIndex][rowIndex]==(" ")) {
                    // Spacing for the cell
                    maze.append("   ");
                } else {
                    maze.append(" ").append(maze2D[columnIndex][rowIndex]).append(" ");
                }

                // When rowIndex is at end AND columnIndex is not at end, add a new line
                if (rowIndex == (size - 1) && columnIndex != (size - 1)) {
                    maze.append(System.lineSeparator());
                }
            }
        }
        return maze.toString();
    }
    public static void main(String[] args) {
        // Repeat Program
        while (true) {
            System.out.println("-------------------------------------------------------------------");
            // User Input for Maze Size
            @SuppressWarnings("resource")
            Scanner scan = new Scanner(System.in);
            int size;

            do {
                System.out.print("Input the size for the maze: ");
                while (!scan.hasNextInt()) {
                    // Repeat message when bad input
                    System.out.println("Needs a valid integer for maze size (3 or Higher)");
                    System.out.print("Input the size for the maze: ");
                    scan.next();
                }
                size = scan.nextInt();
            } while (size <= 2);

            System.out.println();

            // Constructs a new 2D array
//            String[][] maze2D = maze2D(size);

            // Prints out new maze as 2D array
            String[][] mazeGenerated = generator(maze2D(size));


            // Prints the string representation of maze
            System.out.println(convert2D(mazeGenerated));
            System.out.println("String Representation of Generated " + size + "x" + size + " Maze");

            // Delete the Hash symbol in the maze
            mazeGenerated = emptyHash(mazeGenerated);

            // Depth First Search
            String[][] mazeDFS = DFS(clone(mazeGenerated));
            System.out.println();

            // String representation of DFS
            System.out.println(convert2D(mazeDFS));
            System.out.println("String representation of DFS Maze");
            System.out.println();

            // Breadth First Search
            String[][] mazeBFS = BFS(clone(mazeGenerated));
            System.out.println(convert2D(mazeBFS));
            System.out.println("String representation of BFS Maze");
            System.out.println();

            // Creates an single path of the maze
            String[][] mazePath = backtrackingDelete(clone(mazeGenerated));
            emptyHash(mazePath);
            hashList(mazePath);

            // Prints the string representation of maze with path
            System.out.println(printPath(mazePath));
            System.out.println("Hash Single Path");
        }
    }
}